class CreateCrimes < ActiveRecord::Migration
  def change
    create_table :crimes do |t|
      t.string :description
      t.integer :year
      t.integer :month
      t.string :location

      t.timestamps null: false
    end
  end
end
